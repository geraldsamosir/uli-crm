'use strict'

require('dotenv').config()
require('./src/transport/queue')
const WABot = require('./src/lib/whatsapp-js')
const Date = require('./src/config/date')

Date.setTimeZone('Asia/Jakarta')
WABot.run()


console.info('bot has running...')