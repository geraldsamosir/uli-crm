'use strict'

const moment = require('moment-timezone')

class Date {
  /**
   * set time by area
   * 
   * @void
   * @param {string} timeZone 'ex: Asia/Jakarta'
   * 
   */  
  setTimeZone (timeZone) {
    moment.tz(timeZone)
    return
  }
}

module.exports = new Date()


 