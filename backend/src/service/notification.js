'use strict'

const WaBot = require('../lib/whatsapp-js')
const NotificationRepo = require('../repository/notification.repo')
const ConsumenRepo = require('../repository/consumen.repo')

class Notification {

  constructor () {
    this.NotificationRepo = new NotificationRepo()
    this.ConsumenRepo = new ConsumenRepo()
  }

  /**
   * process Notification
   *
   * @param {string} notifId
   * 
   * @returns {Promise<boolean>}
   */
  async processNotification (notifId) {
    const NotificationData = await this.NotificationRepo.getProcessedNotification(notifId)

    if (!NotificationData) return false

    // NotificationData?.ucd_id value ex: [1,2,3]
    const consumenIds = JSON.parse(NotificationData?.ucd_id) || []

    const consumens = await this.ConsumenRepo.getMultipleContact(consumenIds)

    const payload = consumens.map(consumen => {
      let message = this.compileMessage({ ucd_name: consumen.ucd_name}, NotificationData.unt_title)
      message += '\n'
      message += this.compileMessage({ ucd_name: consumen.ucd_name}, NotificationData.unt_body)

      return this.sendMessage(consumen.ucd_msisdn, message, NotificationData.unt_banner_url)
    })

    await Promise.allSettled(payload)

    return true
  }


  /**
   * Compile Message
   * 
   * using replacement using  replace pattern matching 
   * ex: {%user%} using .replaceAll
   * 
   * @param {Object} replaceObject 
   * @param {string} message 
   * 
   * @returns {string}
   */
  compileMessage (replaceObject, message) {
    const replaceMentChars = Object.keys(replaceObject)

    replaceMentChars.forEach(replaceMentChar => {
       message = message.replaceAll(`{%${replaceMentChar}%}`, replaceObject[replaceMentChar])
    })

    return message
  }


  /**
   * Send Message
   * 
   * @param {string} numberTarget 
   * @param {string} message 
   * @param {?string} bannerUrl
   * 
   * @returns  {Promise<boolean>}
   */
  async sendMessage (numberTarget, message, bannerUrl) {
    const numberDetails = await WaBot.Client.getNumberId(numberTarget)

    if (!numberDetails) return false
  
    const media = await WaBot.messageMedia.fromUrl(bannerUrl).catch(err => {
      console.log(bannerUrl, err)
      return null
    })

    if (!media) {
      await WaBot.Client.sendMessage(numberDetails._serialized, message)
      return true
    }

    await WaBot.Client.sendMessage(numberDetails._serialized, media, { caption: message })

    return true
  }
  
}

module.exports = new Notification()
