'use strict'

const NotificationService = require('../notification')
const WaBot = require('../../lib/whatsapp-js')

const sendMessage = jest.spyOn(WaBot.Client,'sendMessage')
const getNumberId = jest.spyOn(WaBot.Client, 'getNumberId')
const downloadMedia = jest.spyOn(WaBot.messageMedia, 'fromUrl')

describe('NotificationService.sendMessage', () => {
    test('should be success', async() => {
        getNumberId.mockResolvedValueOnce(
          {
            _serialized: '62xxxxx'
          }  
        )
        sendMessage.mockResolvedValueOnce(true)
        
        const isSuccess = await NotificationService.sendMessage('62XXX', 'message', 'banner_url')
    
        expect(isSuccess).toEqual(true)
    })

    test('should be success -  with media', async() => {
      getNumberId.mockResolvedValueOnce(
        {
          _serialized: '62xxxxx'
        }  
      )

      downloadMedia.mockResolvedValueOnce(true)

      sendMessage.mockResolvedValueOnce(true)
      
      const isSuccess = await NotificationService.sendMessage('62XXX', 'message', 'banner_url')
  
      expect(isSuccess).toEqual(true)
    })

    test('should be success - return false', async() => {
        getNumberId.mockResolvedValueOnce()
        
        const isSuccess = await NotificationService.sendMessage('62XXX', 'message')
    
        expect(isSuccess).toEqual(false)
    })
})