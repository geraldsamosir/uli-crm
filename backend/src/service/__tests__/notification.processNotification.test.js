'use strict'

const NotificationService = require('../notification')

const getProcessNotification = jest.spyOn(NotificationService.NotificationRepo, 'getProcessedNotification')
const sendMessage = jest.spyOn(NotificationService,'sendMessage')
const getConsumen = jest.spyOn(NotificationService.ConsumenRepo, 'getMultipleContact')

describe('NotificationService.processNotification', () => {
  test('should be succes - return false', async() => {
    getProcessNotification.mockResolvedValueOnce()
    
    const isSuccess = await NotificationService.processNotification('notifId')

    expect(isSuccess).toEqual(false)
  })

  test('should be success', async () => {
    getProcessNotification.mockResolvedValueOnce({
      un_id: 'un_id',
      ucd_id: '[1,2,3,4]',
      unt_body: 'hello man',
      unt_title: 'tittle'
    })

    getConsumen.mockResolvedValueOnce([
      {
        ucd_name: 'ucd_name',
        ucd_msisdn: '62XXXXXXX'
      }
    ])

    sendMessage.mockResolvedValueOnce(true)

    const isSuccess = await NotificationService.processNotification('notifId')

    expect(isSuccess).toEqual(true)
  })
})