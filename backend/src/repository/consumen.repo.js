'use strict'

const { BaseNumberRepo } = require('./base.repo')

class ConsumenRepository extends BaseNumberRepo {
    constructor () {
      super()  
      this.tableName = 'uli_consumen_data'   
      this.tablePrimaryKey = 'ucd_id'
      this.deletedField = null
    }

    /**
     * Get Multiple Contacts
     * 
     * @param {number[]} consumenIds 
     * 
     * @returns {Promise<Object[]>}
     */
    getMultipleContact (consumenIds) {
       return this.database.from(this.tableName)
        .whereIn(`${this.tableName}.${this.tablePrimaryKey}`, consumenIds)
    }
}

module.exports = ConsumenRepository
