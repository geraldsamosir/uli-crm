'use strict'

const { BaseNumberRepo } = require('./base.repo')

class NotificationTemplateRepository extends BaseNumberRepo {
    constructor () {
      super()  
      this.tableName = 'uli_notification_template'   
      this.tablePrimaryKey = 'unt_id'
      this.deletedField = null
    }
}

module.exports = NotificationTemplateRepository
