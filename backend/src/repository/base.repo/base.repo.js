'use strict'

const { Database } = require('../../config')

class BaseRepository {
  constructor () {
    this.database = new Database().getInstance()
    this.tableName = null
    this.tablePrimaryKey = null
    this.deletedField = null // optional field
  }

  /**
    * create data
    *
    * @param {Object} data
    *
    * @returns {boolean}
    */
  async create (data) {
    await this.database.insert(data).into(this.tableName)
    return true
  }
}

module.exports = BaseRepository
