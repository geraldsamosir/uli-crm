'use strict'

const qrcode = require('qrcode-terminal')
const { Client, LocalAuth, MessageMedia } = require('whatsapp-web.js')
const BotRepository = require('../../repository/bot.repo')
const BOT_ID = 1

class Whatsapp {
  constructor () {
    this.messageMedia = MessageMedia
    this.botRepository = new BotRepository()
    if (!this.Client) this.Client = new Client({
      authStrategy: new LocalAuth()
    })
  }

  run () {
    this.Client.on('qr', async (qr) => {
      await this.botRepository.update(
        {
          ubd_id: BOT_ID
        },
        {
        ubd_token: qr  
      })
      
      qrcode.generate(qr, { small: true })
    })

    this.Client.on('ready', async () => {
      console.info('Bot Ready :) ...')
      await this.botRepository.update({
        ubd_id: BOT_ID
      },{
        ubd_status: 1
      })
    })

    this.Client.on('disconnected', async () => {
      console.info('Bot Disconnected :( ...')
      await this.botRepository.update({
        ubd_id: BOT_ID
      },{
        ubd_status: 2
      })
    })

    this.Client.initialize().then(sucess => {
      // do nothing
    })
  }
}

module.exports = new Whatsapp()
