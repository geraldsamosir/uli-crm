'use strict'

const BullQueue = require('bull')
const REDIS_HOST = 'localhost'
const REDIS_PORT = 6379

class Queue extends BullQueue {
   constructor (queueName) {
     super(queueName, {
        host: REDIS_HOST,
        port: REDIS_PORT
     })
     this.name = queueName
   }
}

class ListQueue {
   constructor () {
     this.queues = []
   }

   getQueue (queueName) {
     const connection = this.queues.find(queue => queue.name === queueName)

     if (!connection) return this.addQueue(queueName)
     
     return connection
   }

   addQueue (queueName) {
    const connection = new Queue(queueName)
    this.queues = [...this.queues, connection]
    return connection
   }
}

module.exports = new ListQueue()