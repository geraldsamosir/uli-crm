'use strict'

const ListQueue = require('../../lib/queue')
const BotQueue = ListQueue.getQueue('bot-queue')
const Notification = require('../../service/notification')

BotQueue.process(async(job, done) => {

    if (!job?.data) done()

    const { notifId } = job.data

    try {
      const result = await Notification.processNotification(notifId)
      console.log('result', result)
    } catch (error) {
      console.error('error::', error) 
    }
    done()
})

module.exports = BotQueue
