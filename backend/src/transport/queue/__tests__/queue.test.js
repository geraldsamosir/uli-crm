'use strict'

/**
 * this is just handler local test don run it in test pipeline
 */

const { v4: uuidv4 } = require('uuid');
const ListQueue = require('../../../lib/queue')
const BotQueue = ListQueue.getQueue('bot-queue')


const samples = [
//    uuidv4(),
//    uuidv4(),
   '29c0a190-7ac7-11ee-b0a8-0242ac1d0004'
]
console.log('samples', samples)

const BASE_INTERVAL = 1000

for (const sample of samples) {
    BotQueue.add({ notifId: sample}, {delay: BASE_INTERVAL * sample})  
}
