use db_uli_crm;

create table uli_agent (
  ua_id int(11) unsigned AUTO_INCREMENT NOT NULL,
  primary key(ua_id)
);

create table uli_notification_template (
  unt_id int(11) unsigned AUTO_INCREMENT,
  unt_title varchar(255),
  unt_body text,
  unt_created_at timestamp,
  unt_updated_at timestamp,
  unt_deleted_at timestamp,
  primary key(unt_id),
  FULLTEXT INDEX(unt_title),
  INDEX(unt_deleted_at)
);

create table uli_notification (
  un_id binary(16) DEFAULT (uuid_to_bin(uuid())) NOT NULL,
  ua_id int(11) unsigned comment 'reference uli_agent.ua_id',
  unt_id int(11) unsigned comment 'reference uli_notification_template.unt_id',
  un_status tinyint(2) unsigned  comment'1 active , 2 non active, 3 sended',
  ucd_id JSON,
  un_execute_at timestamp,
  un_created_at timestamp,
  un_updated_at timestamp,
  un_deleted_at timestamp,
  primary key(un_id),
  INDEX(ua_id),
  INDEX(unt_id),
  INDEX(un_status),
  INDEX(un_status),
  INDEX(un_execute_at)
);

create table uli_consumen_data (
  ucd_id int(11) unsigned AUTO_INCREMENT,
  ucd_name varchar(255),
  ucd_country_code varchar(3),
  ucd_phone_code varchar(15),
  ucd_msisdn varchar(20),
  ucd_created_at timestamp,
  ucd_updated_at timestamp,
  ucd_deleted_at timestamp,
  primary key(ucd_id),
  FULLTEXT INDEX(ucd_name),
  INDEX(ucd_msisdn)
);

create table uli_bot_data (
  ubd_id int(11) unsigned AUTO_INCREMENT,
  ubd_phone_number varchar(20),
  ubd_token text,
  ubd_status tinyint(2) comment '1 active , 2 non active',
  primary key(ubd_id),
  UNIQUE(ubd_phone_number)
);

ALTER TABLE uli_notification_template CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE uli_notification_template MODIFY unt_body TEXT CHARSET utf8mb4;

ALTER table uli_notification_template add unt_banner_url text default null  after unt_title;


insert into uli_bot_data (
  ubd_id, 
  ubd_phone_number,
  ubd_token,
  ubd_status 
  ) 
  values (1, '62XXXXXX', NULL, 2 );