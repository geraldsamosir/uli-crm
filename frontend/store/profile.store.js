import { create } from 'zustand'
import { persist, createJSONStorage } from 'zustand/middleware'

export const profileStore = create(persist(
    (set, get) => ({
        profileName: null,
        email: null,
        image: null,
    }),
    {
       name: 'profile-storage', // name of the item in the storage (must be unique)
       storage: createJSONStorage(() => localStorage)
    }
))
  
