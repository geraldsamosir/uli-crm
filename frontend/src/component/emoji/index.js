import { useState } from 'react'
import { Emoji as EmojiData } from './emoji.data'

export  default function Emoji ({ setterEmoji, lastValue, fieldName}) {
  const [isOpen, setIsOpen] = useState(false)  
  const chooseEmoji = (emojiStr) => {
    setterEmoji(fieldName, lastValue + emojiStr)
  }  

  return (
    <div className='collapse bg-base-100 collapse-arrow'>
        <input
          type='checkbox'
          checked={isOpen}
          onChange={()=> setIsOpen(!isOpen)} 
        />
        &nbsp;emoji 😀
        <div className="collapse-content flex fl-wrap w-6 bg-base-100" style={{display: isOpen ?'': 'none'}}>
        {
            EmojiData.map((emoji, index) => {
            return (
                <a
                  key={index} 
                 onClick={() => chooseEmoji(emoji) }>
                  {emoji}
                </a>
            ) 
            })
        }
        </div>
    </div>
  )  
}