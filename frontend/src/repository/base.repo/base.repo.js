'use strict'

const { Database } = require('../../config')

class BaseRepository {
  constructor () {
    this.database = new Database().getInstance()
    this.tableName = null
    this.tablePrimaryKey = null
    this.deletedField = null // optional field
  }

  /**
    * create data
    *
    * @param {Object} data
    *
    * @returns {Promise<boolean>}
    */
  async create (data) {
    await this.database.insert(data).into(this.tableName)
    return true
  }

  /**
   * find all entity 
   * @param {?object} whereObj 
   * 
   * @returns {Promise<boolean>}
   */
  findAll (whereObj) {
    const isDeletedFieldExist = !!this.deletedField
    const query = this.database.from(this.tableName)

    if (whereObj) query.where(whereObj)

    if (isDeletedFieldExist) query.where({ [this.deletedField]: null  })
    
    return query
  }
}

module.exports = BaseRepository
