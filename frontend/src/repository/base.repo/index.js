'use strict'

const BaseNumberRepo = require('./base.number.repo')
const BaseUUIDRepo = require('./base.uuid.repo')

module.exports = { BaseNumberRepo, BaseUUIDRepo }