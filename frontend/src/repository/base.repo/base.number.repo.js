'use strict'

const BaseRepo = require('./base.repo')

class BaseNumberRepository extends BaseRepo {
  constructor () {
    super()
  }

  /**
    * find collection by id
    *
    * @param {string} id - uuid
    * @returns
    */
  async findById (id) {
    const query = this.database.from(this.tableName)
      .select('*')
      .where({[this.tablePrimaryKey]:  id})
    
    if(this.deletedField) query.where({ [this.deletedField]: null })
      
    return await query.first()
  }

  /**
    * find data
    *
    * @param {Object} where - object name that related to table fields
    *
    * @returns {object}
    */
  async find (where) {
    return await this.database
      .select('*')
      .from(this.tableName).where({ ...where, [this.deletedField]: null }).first()
  }

  /**
    * Update data
    *
    * @param {Object} where
    * @param {Object} data
    *
    * @returns {number}
    */
  async update (where, data) {
    const affected = await this.database
        .where({ 
          ...where, 
          ...(
            this.deletedField ?
            {
              [this.deletedField]: null
            }
            : null
          ) 
        })
        .into(this.tableName)
        .update(data)

    return affected
  }

  /**
    * delete data
    *
    * @param {string} id
    *
    * @returns {boolean}
    */
  async delete (id) {
    await this.update(
      { [this.tablePrimaryKey]: id },
      { [this.deletedField]: new Date() }
    )

    return true
  }
}

module.exports = BaseNumberRepository
