'use strict'

const { BaseNumberRepo } = require('./base.repo')

class BotRepository extends BaseNumberRepo {
   constructor () {
     super()
     this.tableName = 'uli_bot_data'   
     this.tablePrimaryKey = 'ubd_id'
     this.deletedField = null
   }
}

module.exports = BotRepository
