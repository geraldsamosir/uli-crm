'use strict'

const { BaseUUIDRepo } = require('./base.repo')
const moment = require('moment')

const notificationStatus = {
   active: 1,
   nonActive: 2,
   sended: 3 
}

class NotificationRepository extends BaseUUIDRepo {
    constructor () {
      super()  
      this.tableName = 'uli_notification'   
      this.tablePrimaryKey = 'un_id'
      this.deletedField = null
    }

    getProcessedNotification (notifId) {
      const now = moment().format('YYYY-MM-DD HH:mm:ss')
      
      return this.database.from(this.tableName)
        .join('uli_notification_template',`${this.tableName}.unt_id`, 'uli_notification_template.unt_id')
        .where({
            [this.tablePrimaryKey]: this.database.raw(`UUID_TO_BIN('${notifId}')`)
        })
        .andWhere(`${this.tableName}.un_status`, notificationStatus.active)
        .andWhere(`${this.tableName}.un_execute_at`, '<=', now)
        .orderBy(`${this.tableName}.un_execute_at`, 'DESC')
        .first()
        .then(result => {
          return result
        })
    }

    /**
    * create data
    *
    * @param {Object} data
    *
    * @returns {Promise<string>}
    */
    async create (notificationId, data) {
      const [binaryId] = await this.database.insert({
        ...data,
        un_id: this.database.raw(`UUID_TO_BIN('${notificationId}')`)
      }).into(this.tableName)

      return binaryId
    }

    async generateUUID () {
      const result = await this.database.raw('select UUID() as UUID')

      return result
    }
}

module.exports = NotificationRepository
