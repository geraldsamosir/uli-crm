import { NextResponse } from 'next/server'
import { redirect } from 'next/navigation'
import { cookies } from 'next/headers'

export async function middleware (req) {
  const { pathname } = req.nextUrl
  const url = req.nextUrl.clone()
  url.pathname = '/login'

  const protectedUrl = [
    '/',
    '/dashboard',
    '/dashboard/contact',
    '/dasboard/template',
    '/api/contact',
    '/api/notification',
    '/api/template'
  ]

  // const cookie = cookies()
  // const csrfToken = cookie.get('next-auth.csrf-token')
  // const authsession = cookie.get('next-auth.session-token')

  // if (protectedUrl.includes(pathname) && !(csrfToken && authsession)) {
  //   return NextResponse.redirect(url)
  // }
   
  return NextResponse.next();
}

export const config = {
  matcher: ['/dashboard/:path*',  '/', '/api/:path*']
}