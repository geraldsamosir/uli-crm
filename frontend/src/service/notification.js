import moment from "moment"
import NotificationRepository from "@/repository/notification.repo"
import ListQueue  from '../lib/queue'
const BotQueue = ListQueue.getQueue('bot-queue')

BotQueue.isReady().then(()=>{
   console.log('bot-queue ready...') 
})

const NotificationInstance = new NotificationRepository()

/**
 * create notifiication
 * 
 * @param {payload} payload
 * @param {number} payload.templateCode
 * @param {number[]} payload.userTargetIds
 * 
 * @returns {Promise<boolean>}
 *  
 */
export const CreateNotification = async ({templateCode, userTargetIds }) => {
  const now = moment().format('YYYY-MM-DD HH:mm:ss')

  const [result] = await NotificationInstance.generateUUID()

  const UUID = result[0].UUID

  const binnaryId = await NotificationInstance.create(UUID, { 
    ua_id: 1,
    un_status: 1,
    unt_id: templateCode,
    ucd_id: JSON.stringify(userTargetIds),
    un_execute_at: now,
    un_created_at: now,
    un_updated_at: now
  })

 await BotQueue.add({ notifId: UUID }, {delay: 1000})
 console.log('success send', UUID)
 return true
}
