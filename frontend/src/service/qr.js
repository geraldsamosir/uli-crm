'use strict'

import moment from 'moment'
import BotRepository from '../repository/bot.repo'
import { NextResponse } from 'next/server'
const botInstance = new BotRepository()
const DEFAULT_ID = 1

export const getQrCode = async () => {
  const botData = await botInstance.findById(DEFAULT_ID)
  const { ubd_token: qrToken } = botData
  return NextResponse.json({ qrToken })
}