'use strict'

import moment from 'moment'
import ConsumenRepository from '../repository/consumen.repo'

const consumenInstance = new ConsumenRepository()

/**
 * 
 * create Contact
 * 
 * @param {object} payload
 * @param {string} payload.fullName
 * @param {string} payload.countryCode
 * @param {string} payload.phoneCode
 * 
 * 
 * @returns {Promise<boolean>}
 */
export const create = async ({fullName, countryCode, phoneCode }) => {
  const timeNow = moment().format('YYYY-MM-DD HH:mm:ss')
  const contactResult = await consumenInstance.create({
    ucd_name: fullName,
    ucd_country_code: countryCode,
    ucd_phone_code: phoneCode,
    ucd_msisdn: `${countryCode}${phoneCode}`,
    ucd_created_at: timeNow,
    ucd_updated_at: timeNow
  })  
  return true
}

/**
 * Find all contact
 * 
 * @returns {Promise<Object[]>}
 */
export const findAll = async ({searchNameOrPhone}) => {
  let contactList = consumenInstance.findAll()
 
  if(searchNameOrPhone) contactList.whereLike('ucd_name', `%${searchNameOrPhone}%`).orWhereLike('ucd_msisdn', `%${`%${searchNameOrPhone}%`}%`)

  contactList = await contactList

  return contactList
}

/**
 * 
 * @param {number} id
 * 
 * @returns {Promise<boolean>}
 */
export const softDelete = async (id) => {
  const contactList = await consumenInstance.delete(id)
  return true
}