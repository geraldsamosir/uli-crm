'use strict'

import moment from 'moment'
import NotificationTmpRepository from '../repository/notification.tmp.repo'

const NotificationTmpInstance = new NotificationTmpRepository()

export const findAll = async () => {
   const notificationTmpList =  await NotificationTmpInstance.findAll()

   return notificationTmpList
}


export const createTemplate = async (title, content, bannerUrl) => {
 const now = moment().format('YYYY-MM-DD HH:mm:ss')

 await NotificationTmpInstance.create({
   unt_title: title,
   unt_banner_url: bannerUrl,
   unt_body: content,
   unt_created_at: now,
   unt_updated_at: now
 })

 return true
}