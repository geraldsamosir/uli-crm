'use client'

import React from 'react'
import { useQRCode } from 'next-qrcode'
 
export default function QRCode({ qrToken }) {
  const { Canvas } = useQRCode()

  return (
    <Canvas
      text={qrToken}
      options={{
        type: 'image/jpeg',
        quality: 0.3,
        errorCorrectionLevel: 'M',
        margin: 3,
        scale: 4,
        width: 200,
        color: {
          dark: '#010599FF',
          light: '#FFBF60FF',
        },
      }}
    />
  )
}
 
