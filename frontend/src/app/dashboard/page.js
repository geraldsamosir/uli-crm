"use client"

import "./page.css";
import { Formik, Field, Form, select } from 'formik'
import Select from 'react-select'
import makeAnimated from 'react-select/animated'
import NavigationComponent from './navigation'
import { useEffect, useState } from 'react'
import { GET_LIST_CONTACT } from "./contact/action"
import { GET_LIST_NOTIF_TMP } from "./template/action"
import { CREATE_NOTIFICATION } from "./notification/action"
import * as Yup from 'yup'

const notificationSchema = Yup.object().shape({
  templateCode: Yup.string().required('Required'),
  userTargetId: Yup.array().required('Required')
})

export default function DashboardHome () {
  const [listContacts, setListContacts] = useState([])
  const [listNotifTMP, setListNotifTMP] = useState([])

  useEffect(() => {
    async function getData () {
      let listContact = await GET_LIST_CONTACT()
      const listNotifTMP = await GET_LIST_NOTIF_TMP()
      // const listTemplate = await 
      listContact = listContact.map(contact => {
        return {
          label: contact.fullName,
          value: contact.id
        }
      })
      setListContacts(listContact)
      setListNotifTMP(listNotifTMP)
    }

    getData()
    
  }, [listContacts, listNotifTMP])
   
   return (
    <div className="container w-full h-full flex-col">
      <NavigationComponent/>
     <div>
      <h1>
         Welcome to ULI dashboard
      </h1>
      <hr/>
     </div>
     <Formik
         initialValues={{
            templateCode: '',
            userTargetId: ''
          }}
          validationSchema={notificationSchema}
          onSubmit={async (values) => {
            const {templateCode, userTargetId } = values
            const userTargetIds = userTargetId.map(target => target.value)

            const payload = { templateCode, userTargetIds}

            await CREATE_NOTIFICATION(payload)
          }}
        >
        {
            ({ isSubmitting, setFieldValue }) => (
                <Form className='flex flex-wrap space-x-1 m-5'>
                    <Field
                    as='select'
                    name='templateCode'
                    className="browser-default select w-1/12 relative m-0 block w-[1px] min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none dark:border-neutral-600 dark:focus:border-primary"
                    >
                     <option defaultValue='selected'>Template Code</option>  
                     {
                       listNotifTMP.map(tmp => {
                        return(
                          <option key={tmp.id} id={tmp.id} value={tmp.id}>{tmp.title}</option>
                        )
                       })
                     }
                    </Field>
                    
                    <Select
                     as='select'
                     name='userTargetId'
                     className='my-react-select-container browser-default select w-1/12 relative m-0 block w-[1px] min-w-0 flex-auto rounded bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] '
                     isMulti
                     classNamePrefix="my-react-select"
                     options={listContacts}
                     onChange={(value) => setFieldValue('userTargetId', value)}
                    />
                    <button className='btn btn-primary' type="submit" disabled={isSubmitting}>Submit</button>
                </Form>
            )
        }
    </Formik>
    </div>
   ) 
}