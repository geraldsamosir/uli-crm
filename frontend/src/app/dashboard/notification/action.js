/**
 * Create contact
 * 
 * @param {Object} notification
 * @param {string} notification.templateCode
 * @param {string} notification.userTargetIds
 * 
 * @returns {<Promise<Object>>}
 */
export const CREATE_NOTIFICATION = (notification) => {
    return fetch('/api/notification', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(notification)
    }).then(async (resultContact) => {
      return await resultContact.json()
    })
    .catch(err => {
      console.error(err) 
    })
}