"use client"

import Image  from "next/image"
import { useSession } from "next-auth/react"

export default  function NavigationComponent() {
  const { data: session } = useSession()

  return (
    <div className="navbar bg-base-100">
    <div className="flex-1">
        <a href='/dashboard' className="btn btn-ghost normal-case text-xl">ULI CRM 🌹</a>
        <a href='/dashboard/contact' className="btn btn-ghost normal-case text-l">Contact</a>
        <a href='/dashboard/template' className="btn btn-ghost normal-case text-l">Template</a>
    </div>
    <div className="flex-none gap-2">
        <div className="dropdown dropdown-end">
        <label tabIndex={0} className="btn btn-ghost btn-circle avatar">
            <div className="w-10 rounded-full">
            {
              session?.user?.image && <Image src={session?.user?.image} alt="" width={10} height={5}/>
            }
            </div>
        </label>
        <ul tabIndex={0} className="mt-3 z-[1] p-2 shadow menu menu-sm dropdown-content bg-base-100 rounded-box w-52">
            <li key={1}>
            <a className="justify-between">
            </a>
            </li>
            <li key={2}><a href='/api/auth/signout'>Logout</a></li>
        </ul>
        </div>
    </div>
    </div>
  )
}