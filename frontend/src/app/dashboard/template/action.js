
export const GET_LIST_NOTIF_TMP = () => {
    return fetch('/api/template', {
       method: 'GET',
       headers: {
          'Content-Type': 'application/json'
       } 
    }).then(async (resultListNotifTMP) => {
        const listNotifTMP = await resultListNotifTMP.json()

        return listNotifTMP
    }).catch(err => {
        console.log(err)
    })
}

export const UPLOAD_BANNER = (file) => {
    const formData = new FormData()
    formData.append('file', file)

    return fetch('/api/upload', {
        method: 'POST',
        body: formData
    }).then(async  (bannerResult) => {
        const { secure_url: secureUrl, url } = await bannerResult.json()
        return { secureUrl, url }
    }).catch(err => {
        console.log(err)
    })
}

export const CREATE_TEMPLATE = ({title, bannerUrl, content}) => {
   const payload = {
     title,
     bannerUrl,
     content
   }
   return fetch('/api/template', {
    method: 'POST',
    headers: {
       'Content-Type': 'application/json'
    },
    body: JSON.stringify(payload)
    }).then(async (resultNotifTMP) => {
        return await resultNotifTMP.json()
    }).catch(err => {
        console.log(err)
    })
}
