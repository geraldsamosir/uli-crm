import NavigationComponent from '../navigation'

export default function RootLayout({
  children,
  modal,
}) {
  return (
    <div>
      <NavigationComponent/>  
      <>{children}</>
    </div>
  );
}
