'use client'

import { Formik, Field, Form } from 'formik'
import { CREATE_TEMPLATE, UPLOAD_BANNER } from './action'
import { useState } from 'react'
import Emoji from '../../../component/emoji'
import * as Yup from 'yup'

const templateSchema = Yup.object().shape({
  title: Yup.string().required('Required'),
  content: Yup.string().required('Required'),
  bannerUrl: Yup.string().required('Required')
})

export default function Template () {
  const [imageUploadUrl, setImageUploadUrl] = useState('')
  return (
    <div className="container overflow-x-auto">
      <Formik
        initialValues={{
          title: '',
          content: '',
          bannerUrl: ''
        }}
        validationSchema={templateSchema}
        onSubmit={async (values, { resetForm }) => {
          await CREATE_TEMPLATE(values)
          resetForm()
          setImageUploadUrl('')
        }}>
       {
          ({values, isSubmitting, setFieldValue, handleChange }) => (
            <Form className='flex flex-col p-2 space-y-8'>
              <Field
                  className='relative m-0 block  flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none'
                  name='title'
                  type='text'
                  placeholder='Title notification'
              />
              <>
                <h3>Banner: </h3>
                <Field
                  name='file'
                  type='file'
                  placeholder='Choose Banner'
                  onChange={ async (e)=> {
                    const { secureUrl } = await UPLOAD_BANNER(e.target.files[0])

                    setImageUploadUrl(secureUrl)
                    setFieldValue('bannerUrl', secureUrl)
                  }}
                />
              </>
              <img src={imageUploadUrl} className='h-auto max-w-ful'/>
              <Emoji setterEmoji={setFieldValue} lastValue={values.content} fieldName={'content'}/>
              <textarea
                  name='content'
                  cols={10}
                  rows={5}
                  type='text'
                  className='relative m-0 block  flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none'
                  placeholder={'Notification content'} 
                  onChange={handleChange}
                  value={values.content}
              />
              <button className='btn btn-primary' type="submit" disabled={isSubmitting}>Submit</button>
            </Form>
          )
       }
      </Formik>
    </div>
  )  
}