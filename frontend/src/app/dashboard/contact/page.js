'use client'

import Image from 'next/image'
import { Formik, Field, Form, select } from 'formik'
import { CREATE_CONTACT, GET_LIST_CONTACT, DELETE_CONTACT } from './action'
import { useEffect, useState } from 'react'
import * as Yup from 'yup'

const contactSchema = Yup.object().shape({
  fullName: Yup.string().required('Required'),
  countryCode: Yup.string().required('Required'),
  phoneCode: Yup.string().required('Required')
})

export default function Contact () {
    const [listContacts, setListContacts] = useState([])

    useEffect(() => {
      async function fetchContactList () {
        const listContact = await GET_LIST_CONTACT() 
        setListContacts(listContact)
      }  
      fetchContactList()
    }, [listContacts])

    return (
       <div className="overflow-x-auto">
        <Formik
         initialValues={{
            fullName: '',
            countryCode: '',
            phoneCode: ''
          }}
          validationSchema={contactSchema}
          onSubmit={async (values) => {
            await CREATE_CONTACT(values)
            const listContact = await GET_LIST_CONTACT()
            setListContacts(listContact)
          }}
        >
        {
            ({ isSubmitting }) => (
                <Form className='flex flex-wrap space-x-1'>
                    <Field name='fullName' className='w-5/12 relative m-0 block w-[1px] min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none dark:border-neutral-600 dark:focus:border-primary' placeholder={'full name'}/>
                    <Field
                    as="select"
                    name='countryCode'
                    className="browser-default select w-1/12 relative m-0 block w-[1px] min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none dark:border-neutral-600 dark:focus:border-primary"
                    >
                    <option defaultValue={true}>Country Code</option>
                    <option id='+62' value='62'>+62</option>
                    <option id='+65' value="65">+65</option>
                    </Field>

                    <Field className={'w-4/12 relative m-0 block w-[1px] min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none dark:border-neutral-600 dark:focus:border-primary'} type="number" name='phoneCode' placeholder="Phone Code"/>
                    <button className='btn btn-primary' type="submit" disabled={isSubmitting}>Submit</button>
                </Form>
            )
        }
        </Formik>
        <br/>
        <div className="mb-3">
        <div className="relative mb-4 flex w-1/3 flex-wrap items-stretch text-white">
            <input
            type="search"
            className=" relative m-0 block w-[1px] min-w-0 flex-auto rounded border border-solid border-neutral-300 bg-transparent bg-clip-padding px-3 py-[0.25rem] text-base font-normal leading-[1.6] text-white-700 outline-none transition duration-200 ease-in-out focus:z-[3] focus:border-primary focus:text-white-700 focus:shadow-[inset_0_0_0_1px_rgb(59,113,202)] focus:outline-none dark:border-neutral-600 dark:focus:border-primary"
            placeholder="Search name or phone number"
            aria-label="Search"
            aria-describedby="button-addon2" 
            onChange={async (event) => {
                const listContacts = await GET_LIST_CONTACT({searchNameOrPhone: event.target.value})
                setListContacts(listContacts)
             }}/>

            <span
            fillRule="input-group-text flex items-center whitespace-nowrap rounded px-3 py-1.5 text-center text-base font-normal text-neutral-700 dark:text-neutral-200"
            id="basic-addon2">
            <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 20 20"
                fill="currentColor"
                className="h-5 w-5">
                <path
                fillRule="evenodd"
                d="M9 3.5a5.5 5.5 0 100 11 5.5 5.5 0 000-11zM2 9a7 7 0 1112.452 4.391l3.328 3.329a.75.75 0 11-1.06 1.06l-3.329-3.328A7 7 0 012 9z"
                clipRule="evenodd" />
            </svg>
            </span>
        </div>
        </div>
        <br/>
        <table className="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            {
                listContacts.map(contact => {
                   return (
                    <tr key={contact.msisdn}>
                        <td>
                        <div className="flex items-center space-x-3">
                            <div className="avatar">
                            <div className="mask mask-squircle w-12 h-12">
                                <Image src={`https://ui-avatars.com/api/?name=${contact.fullName}`} alt="Avatar Tailwind CSS Component" width={100} height={100}/>
                            </div>
                            </div>
                            <div>
                            <div className="font-bold">{contact.fullName}</div>
                            </div>
                        </div>
                        </td>
                        <td>
                         {contact.msisdn}
                        </td>
                        <td>
                            <button onClick={async ()=> {
                                await DELETE_CONTACT(contact.id)
                                const listContact = await GET_LIST_CONTACT()
                                setListContacts(listContact)
                            }}>
                                delete
                            </button>
                            </td>
                    </tr>
                   )
                })
            } 
            
            </tbody>
            <tfoot>
            <tr>
                <th>Name</th>
                <th>Phone</th>
                <th></th>
            </tr>
            </tfoot>
        </table>
    </div> 
    )
}