/**
 * Create contact
 * 
 * @param {Object} contact
 * @param {string} contact.fullName
 * @param {string} contact.countryCode
 * @param {string} contact.phoneCode
 * 
 * @returns {<Promise<Object>>}
 */
export const CREATE_CONTACT = (contact) => {
  return fetch('/api/contact', {
    method: 'POST',
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(contact)
  }).then(async (resultContact) => {
    return await resultContact.json()
  })
  .catch(err => {
    console.error(err) 
  })
}

/**
 * Get List Contact
 * 
 * @param {Object} filter
 * @param {string} filter.searchNameOrPhone
 *  
 * @returns {Promise<Object[]>}
 */
export const GET_LIST_CONTACT = (filter = {}) => {
  const queryObject = new URLSearchParams(filter)

  const listContactUrl = Object.keys(filter).length > 0 ? `/api/contact?${queryObject.toString()}` : '/api/contact'
  return fetch(listContactUrl, {
    method: 'GET',
    headers: {
      "Content-Type": "application/json"
    }
  }).then(async (resultListContacts) => {
    const listcontacts = await resultListContacts.json()
    return listcontacts
  }).catch(err => {
    console.error(err)
  })
}

/**
 * Delete contact
 * 
 * @param {number} id
 *  
 * @returns {Promise<boolean>}
 */
export const DELETE_CONTACT = (id) => {
  return fetch(`/api/contact?id=${id}`, {
    method: 'DELETE',
    headers: {
      "Content-Type": "application/json"
    }
  }).then(() => {
    return true
  }).catch(err => {
    console.error(err)
  })
}