'use client'

import { signIn } from 'next-auth/react'
import Image from 'next/image'
import RosesComponent from './roses'

export default function LoginPage() {
    return (
      <>
      <section className="login">
        <div className="z-10 pt-20">
          <h2 className="text-white">
            ULI CRM
          </h2>
          <h3 className="text-white mt-56">Sign in to your account</h3>
          <button className="text-white" onClick={() => signIn("google", { callbackUrl: "/" })}>
            <img src="/google.svg" alt="Google Logo" width={24} height={24} />
            Sign in with Google
          </button>
        </div>
      </section>
      <RosesComponent/>
      </>
    )
  }

