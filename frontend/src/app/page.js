import { getServerSession } from 'next-auth/next'
import { authOptions } from '@/app/api/auth/[...nextauth]/route'
import { redirect } from 'next/navigation'
import QRCode from "./qr"
import { profileStore } from "../../store/profile.store"
import { getQrCode } from '@/service/qr'

export default async function Home() {
    // Get user session token
    const session = await getServerSession(authOptions)
    let botData = await getQrCode()
    botData = await botData.json()
    const { qrToken } = botData

    if (!session) redirect('/login')

    if (qrToken) redirect('/dashboard')

    const  { user } = session
    profileStore.setState({ profileName: user.name, email: user.email, image:  user.image})

    return (
      <div className="flex h-screen ">
        <center className="m-auto">
        {session && (
          <>
          <div className="mb-5">
            <h2>ULI CRM 🌹</h2>
            <p>Hello  {session.user && session.user.name}  👋</p>
            <p>to continue you can scan this barcode or Signout </p>
            <br/>
            <a href="/api/auth/signout" className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">Signout</a>
          </div>
          <QRCode qrToken={qrToken}/>
          </>
        )}
  
        {!session && (
          <p>Not signed in</p>
        )}
        </center>
      </div>
    )
}
