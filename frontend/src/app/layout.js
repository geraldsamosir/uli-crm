import "./globals.css";

import { Inter } from "next/font/google";
import { Providers } from "./providers";

const inter = Inter({subsets: ["latin"]});

export const metadata = {
  title: "Home Page",
  description: "Generated by create next app",
};

export default function RootLayout({
  children,
  modal,
}) {
  return (
    <html lang="en">
      <body className={`{inter.className} flex items-center justify-center`}>
        <div className="container">
          <Providers>{children}</Providers>
        </div>
        {modal}
      </body>
    </html>
  );
}
