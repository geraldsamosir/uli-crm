import { NextResponse } from 'next/server'
import { v2 as cloudinary } from 'cloudinary';


cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
})


export async function POST (request) {
  const data = await request.formData()
  const file = data.get('file')

  const arrayBuffer = await file.arrayBuffer()
  const buffer = new Uint8Array(arrayBuffer)

  const results = await new Promise((resolve, reject) => {
    cloudinary.uploader.upload_stream({
      tags: ['banner-uli-crm']
    }, function (error, result) {
      if (error) {
        console.log('UPLOAD_ERR: ', error.name)
        return reject(error);
        
      }
      resolve(result);
    })
    .end(buffer);
  });
    
  return NextResponse.json({...results})
 }