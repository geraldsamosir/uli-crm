import NextAuth, { NextAuthOptions } from "next-auth"
import GoogleProvider from "next-auth/providers/google"

export const authOptions = {
   providers: [
     GoogleProvider({
        clientId: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_SECRET_ID,
      }),
    ],
   secret: process.env.GOOGLE_SECRET_ID,
   callbacks: {
    async signIn ({ account, profile }) {
      if (account.provider === "google") {
        return profile.email_verified && profile.email.endsWith(`@${process.env.COMPANY_EMAIL_DOMAIN}`)
      }
      return false
    }
  },
}
  
const handler = NextAuth(authOptions)
export { handler as GET, handler as POST }
  