import { NextResponse } from 'next/server'

import { create as CreateContact, findAll, softDelete } from '../../../service/contact'

export async function POST (request) {
  const data = await request.json()
  
  // TO DO: validation here 


  const {fullName, countryCode, phoneCode } = data

  await CreateContact({fullName, countryCode, phoneCode})

  return NextResponse.json(true)
}

export async function GET (request) {
  const queryUrl = request.nextUrl.searchParams
  const query = queryUrl.get('searchNameOrPhone')

  let listContacts = await findAll({searchNameOrPhone: query})
  listContacts = listContacts.map(listContact => {
    return {
      id: listContact.ucd_id,
      fullName: listContact.ucd_name,
      phoneCode: listContact.ucd_phone_code,
      countryCode: listContact.ucd_country_code,
      msisdn: listContact.ucd_msisdn
    }
  })

  return NextResponse.json(listContacts)
}

export async function DELETE (request) {
  const queryUrl = request.nextUrl.searchParams
  const id = queryUrl.get('id')
  
  await softDelete(id)

  return NextResponse.json(true)
}