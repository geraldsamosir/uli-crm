import { CreateNotification } from "@/service/notification";
import { NextResponse } from "next/server";

export async function  POST (request) {
  const data = await request.json()
  
  // TODO: validation here

  const { templateCode, userTargetIds } = data

  await CreateNotification({ templateCode, userTargetIds })

  return NextResponse.json(true)

}