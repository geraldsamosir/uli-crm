import { NextResponse } from 'next/server'

import { findAll, createTemplate } from '../../../service/notification.tmp'

export async function GET (request) {
   let listNotificationTmp = await findAll()
   listNotificationTmp = listNotificationTmp.map(notificaitonTmp => {
      return {
        id: notificaitonTmp.unt_id,
        title: notificaitonTmp.unt_title,
        banner_url: notificaitonTmp.unt_banner_url,
        body: notificaitonTmp.unt_body
      }
   })

   return NextResponse.json(listNotificationTmp)
}

export async function POST (request) {
   const data = await request.json()

   // TODO: validation here

   const { title, content, bannerUrl } = data

   console.log('here', data)

  await createTemplate(title, content, bannerUrl)

   return NextResponse.json({message: true})
}