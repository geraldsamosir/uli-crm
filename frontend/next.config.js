/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  experimental: {
    serverComponentsExternalPackages: ['knex', 'bull', 'formidable'],
  },
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'lh3.googleusercontent.com',
        port: ''
      },
      {
        protocol: 'https',
        hostname: 'ui-avatars.com',
        port: ''
      },

      {
        protocol: 'https',
        hostname: 'res.cloudinary.com',
        port: ''
      }
    ],
  },
}

module.exports = nextConfig
