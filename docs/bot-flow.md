```mermaid
flowchart TD
    Start -->|incoming message from redis| processValidate
    processValidate -->|get data is notification active or not| db[(Database)]
    db[(Database)] -->|response data| processValidate
    processValidate --> checkdata{is valid datetime <= 1 hours <br/> and notification is active?}
    checkdata -- yes --> processMapping
    checkdata -- no --> stop
    processMapping -->|get notification data, template and user data| db[(Database)]
    db[(Database)] -->| return data| processMapping
    processMapping --> processSending
    processSending --> processSendWhatsapp
    processSendWhatsapp --> processUpdateData
    processUpdateData --> processNotifyWebsite
    processNotifyWebsite --> stop

```