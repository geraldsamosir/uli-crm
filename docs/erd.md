# ERD Diagram

```mermaid

erDiagram
    uli_agent {
       int(11) ua_id PK "auto increment unsigned"
       
    }

    uli_bot_data {
        varchar(20) ubd_phone_number PK
        int(11) ua_id PK "uli_agent.ua_id"
        text ubd_token 
        tinyint(2) ubd_status "1 active , 2 non active (index)"
    }

    uli_consumen_data {
        int(11) ucd_id PK "auto increment unsigned"
        varchar(255) ucd_name "(index full text)"
        varchar(3) ucd_country_code "(index) refernce https://en.wikipedia.org/wiki/List_of_mobile_telephone_prefixes_by_country"
        varchar(15) ucd_phone_code 
        varchar(15) ucd_msisdn "(index)"
        timestamp ucd_created_at "(index)"
        timestamp ucd_updated_at "(index)"
    }

    uli_notification_template {
       int(11) unt_id PK "auto increment unsigned"
       varchar(255) unt_title
       text unt_banner_url
       text unt_body
       timestamp unt_created_at "(index)"
       timestamp unt_updated_at "(index)"
    }

    uli_notification {
       binary(16) un_id PK "uuid to bin"
       int(11) ua_id FK "uli_agent.ua_id"
       int(11) unt_id FK "uli_notification_template.ucd_id"
       tinyint(2) un_status "1 active , 2 non active, 3 sended(index)"
       varchar(255) ucd_id "targeted user ex 1,2,3,4.."
       timestamp un_execute_at "allow null (index)"
       timestamp ut_created_at "(index)"
       timestamp ut_updated_at "(index)"
    }   

    uli_agent ||--o{ uli_bot_data : created
    uli_agent ||--o{ uli_notification: created 
    uli_notification_template ||--o{ uli_notification: used
    uli_notification ||--o{ uli_consumen_data: targeted

```