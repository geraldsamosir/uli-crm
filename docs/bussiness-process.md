# Feature:

1. Register your consumen Whatsapp
2. Templated to send notification to whatsapp
3. send notification (scheduled or just send it)

# 1. Register Your Consumen Whatsapp

```mermaid

sequenceDiagram
    participant us as agent
    participant web as website
    participant db as database
    us ->> web: access contact page and
    web ->> us: give the page
    us ->> web: submit contact form <br/> to add consumen whatsapp
    web ->> db: add new contact form to database
    db ->> web: success
    web ->> us: show to user that contact has added 
```
# 2. Templated to send notification to whatsapp

```mermaid
sequenceDiagram
    participant us as agent
    participant web as website
    participant db as database
    us ->> web: access contact page and
    web ->> us: give the page
    us ->> web: submit template notification template
    web ->> db: add new template form to database
    db ->> web: success
    web ->> us: show to user that template has added 
```

# 3. Send notification (scheduled or just send it)

```mermaid
sequenceDiagram
    participant us as agent
    participant web as website
    participant db as database
    participant rd as redis
    participant be as bot engine
    participant wa as whatsapp

    us ->> web: to make notification
    web ->> db: get template and user list to target
    db ->> web: give the data
    web ->> us:  give the data
    us ->> web: submit the final <br/> template and user targeted
    web ->> database: save the data
    web ->> redis: send queue (in case direct or scheduling)
    redis ->> be : trigger that notification will send 
    be ->> db: check the date of scheduled is passed or not <br/> with tolerant 1 hour before or after
    be ->> be: compose the message before sending
    be ->> wa: send the message
    be ->> db: change flag that message has send or not
    be ->> redis: notify that wa message has process
    redis ->> web: notify that wa message has process
    web ->> us: notify that wa message has process

```

